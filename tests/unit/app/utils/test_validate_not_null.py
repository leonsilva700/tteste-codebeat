import unittest
from unittest.mock import MagicMock, patch
from app.utils.validate_not_null import validate_not_empty_or_none, validate_not_null_object, validate_not_null_dict


class TestValidateNotNull(unittest.TestCase):

    @patch('app.utils.validate_not_null.validate_not_empty_or_none')
    def test_must_return_true_for_dict_with_attribute(self, mock_validate):
        mock = MagicMock()
        mock.return_value = {'teste': 'teste'}
        mock_validate.return_value = True
        self.assertTrue(validate_not_null_dict(['teste'], mock))

    @patch('app.utils.validate_not_null.validate_not_empty_or_none')
    def test_must_return_false_for_dict_with_empty_or_no_attribute(self, mock_validate):
        mock = MagicMock()
        mock.return_value = {'teste': None}
        mock_validate.return_value = False
        self.assertFalse(validate_not_null_dict(['teste'], mock))

    @patch('app.utils.validate_not_null.validate_not_empty_or_none')
    def test_must_return_true_for_obj_with_attribute(self, mock_validate):
        mock = MagicMock()
        mock.__iter__.return_value = ['teste', 10]
        mock_validate.return_value = True
        self.assertTrue(validate_not_null_object(mock))

    @patch('app.utils.validate_not_null.validate_not_empty_or_none')
    def test_must_return_false_for_obj_empty(self, mock_validate):
        mock = MagicMock()
        mock.__iter__.return_value = [None, '']
        mock_validate.return_value = False
        self.assertFalse(validate_not_null_object(mock))

    def test_must_return_false_for_empty_string_or_none_or_integer_value_when_0(self):
        self.assertFalse(validate_not_empty_or_none(''))
        self.assertFalse(validate_not_empty_or_none(None))
        self.assertFalse(validate_not_empty_or_none(0))

    def test_must_return_true_for_filled_value(self):
        self.assertTrue(validate_not_empty_or_none('teste'))
        self.assertTrue(validate_not_empty_or_none(10))