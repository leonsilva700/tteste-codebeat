import unittest
from app.utils.regex_codes import regex_only_numbers


class TestRegexCodes(unittest.TestCase):
    def test_must_return_a_string_of_length_5_with_numbers_only(self):
        result = regex_only_numbers('4j9s-1@12')
        self.assertEqual(len(result), 5)
        self.assertTrue(result.isnumeric())

