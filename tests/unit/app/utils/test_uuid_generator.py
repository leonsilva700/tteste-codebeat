import unittest
from unittest.mock import patch, Mock, create_autospec
from app.utils.uuid_generator import uuid4_generator


class TestUuidGenerator(unittest.TestCase):
    @patch('app.utils.uuid_generator.uuid4')
    def test_must_return_a_string_of_length_36_in_str_format(self, mock_uuid):
        mock_uuid.return_value = '5ad28ced-f5bb-410b-8224-7cab60951b4d'
        result = uuid4_generator()
        mock_uuid.assert_called_once()
        self.assertEqual(len(result), 36)
        self.assertEqual(type(result), str)