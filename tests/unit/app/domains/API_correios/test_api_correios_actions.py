import unittest
from unittest.mock import patch, Mock, MagicMock

from pycep_correios.excecoes import ExcecaoPyCEPCorreios

from app.domains.API_correios.actions import get_address_by_cep_number
from app.domains.API_correios.exceptions import CheckCepnotavailableException


class TestApiCorreiosActions(unittest.TestCase):
    @patch('app.domains.API_correios.actions.pycep_correios')
    def test_if_api_correios_is_returning_the_address(self, mock_consultar_cep):
        cep = '89041450'
        return_mock = {"bairro": "Velha",
                      "cep": "89041450",
                      "cidade": "Blumenau",
                      "complemento2": None,
                      "end": "Rua Reinoldo Althoff",
                      "uf": "SC",
                      "unidadesPostagem": []}
        mock_consultar_cep.consultar_cep = MagicMock(return_value = return_mock)
        get_address_by_cep_number(cep)
        mock_consultar_cep.consultar_cep.assert_called_once_with(cep)

    @patch('app.domains.API_correios.actions.pycep_correios')
    def test_if_api_correios_is_returning_the_exception(self, mock_consultar_cep):
        cep = '8904140'
        mock_consultar_cep.consultar_cep= MagicMock(side_effect= ExcecaoPyCEPCorreios())
        with self.assertRaises(CheckCepnotavailableException) as exc:
            get_address_by_cep_number(cep)
        exc = exc.exception
        self.assertEqual(exc.description,'O CEP está errado')
