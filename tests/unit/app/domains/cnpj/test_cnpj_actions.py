import unittest
from unittest.mock import patch
from app.domains.cnpj.actions import create


class CNPJActionsTest(unittest.TestCase):

    @patch('app.domains.cnpj.actions.CNPJ')
    def test_if_CNPJ_is_called(self, mock_CNPJ):
        create('10.038.536/0001-50')
        mock_CNPJ.assert_called_once_with('10.038.536/0001-50')
