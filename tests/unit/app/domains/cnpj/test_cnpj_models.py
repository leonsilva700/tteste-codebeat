import unittest
import pytest
from app.domains.cnpj.exceptions import InvalidCNPJException, EmptyCNPJException
from app.domains.cnpj.models import CNPJ


class CNPJModelsTest(unittest.TestCase):

    def test_message_exception_is_invalid_cnpj_and_code_is_422(self):
        with pytest.raises(InvalidCNPJException) as contend:
            CNPJ('bghgjh')
        self.assertEqual(contend.value.description, 'Invalid CNPJ')
        self.assertEqual(contend.value.code, 422)

    def test_message_exception_is_empty_cnpj_and_code_is_422_when_passed_empty_cnpj(self):
        with pytest.raises(EmptyCNPJException) as contend:
            CNPJ('      ')
        self.assertEqual(contend.value.description, 'Empty CNPJ')
        self.assertEqual(contend.value.code, 422)

    def test_cnpj_is_saved_when_instantiated(self):
        cnpj = CNPJ('10.038.536/0001-50')

        self.assertEqual(cnpj.get_value(), '10038536000150')

    def test_message_exception_is_empty_cnpj_and_code_is_422_when_passed_none_cnpj(self):
        with pytest.raises(EmptyCNPJException) as contend:
            CNPJ(None)
        self.assertEqual(contend.value.description, 'Empty CNPJ')
        self.assertEqual(contend.value.code, 422)
