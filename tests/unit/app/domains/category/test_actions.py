import unittest
from unittest.mock import patch, MagicMock, Mock
from app.domains.category.actions import create, get, get_by_id, update
from app.domains.category.models import Category


class TestCategoryActions(unittest.TestCase):

    @patch('app.domains.category.actions.Category')
    @patch('app.domains.category.actions.save')
    def test_action_create_should_create_new_category(self, save_mock, mock_class_category):
        create({'name': 'Achocolatado'})
        calls = save_mock.call_args
        args = calls[0]
        category = args[0]
        save_mock.assert_called_once_with(category)
        mock_class_category.assert_called_once_with(name='Achocolatado')

    @patch('app.domains.category.actions.Category')
    def test_action_get_by_id_should_return_category(self, category_mock):
        category_save = Mock()
        query = Mock()
        query.get = MagicMock(return_value=category_save)
        category_mock.query = query
        category = get_by_id('id')
        category_mock.query.get.assert_called_once_with('id')
        self.assertIsNotNone(category)

    @patch('app.domains.category.actions.Category')
    def test_action_get_should_return_category(self, category_mock):
        category_saved = Mock()
        query = Mock()
        query.all = MagicMock(return_value=[category_saved])
        category_mock.query = query
        category = get()
        category_mock.query.all.assert_called_once()
        self.assertEqual(len(category), 1)

    @patch('app.domains.category.actions.get_by_id')
    @patch('app.domains.category.actions.commit')
    def test_action_update_should_update_category_name(self, commit_mock, get_by_id_mock):
        category_saved = Mock()
        category_saved.name = 'Achocolatado'
        get_by_id_mock.return_value = category_saved
        category = update('id', {'name': 'Bebidas'})
        get_by_id_mock.assert_called_once_with('id')
        commit_mock.assert_called_once()
        self.assertEqual(category.name, 'Bebidas')

    @patch('app.domains.category.actions.get_by_id')
    @patch('app.domains.category.actions.commit')
    def test_action_update_should_updated_category_active_to_false(self, commit_mock, get_by_id_mock):
        category_saved = Mock()
        category_saved.active = True
        get_by_id_mock.return_value = category_saved
        category = update('id', {'active': False})
        get_by_id_mock.assert_called_once_with('id')
        commit_mock.assert_called_once()
        self.assertEqual(category.active, False)
