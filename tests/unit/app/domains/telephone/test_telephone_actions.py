import unittest
from unittest.mock import patch, Mock, MagicMock

from app.domains.telephone.actions import create as create_phone, get_by_id_person, get as get_all,update as update_tel

from app.domains.telephone.models import Telephone


class TestTelephoneActions(unittest.TestCase):
    @patch('app.domains.telephone.actions.save')
    def test_action_create_new_telephone(self, save_mock):
        create_phone({'id_person':'456789','number':'4796968787','type_number':'comercial','person_type':'fornecedor'})
        calls = save_mock.call_args
        args = calls[0]
        telephone = args[0]
        self.assertIsInstance(telephone, Telephone)
        self.assertEqual(telephone.id_person, '456789')
        self.assertEqual(telephone.number, '4796968787')
        self.assertEqual(telephone.type_number, 'comercial')
        self.assertEqual(telephone.person_type, 'fornecedor')
        save_mock.assert_called_once_with(telephone)


    @patch('app.domains.telephone.actions.Telephone')
    def test_action_get_by_id_person(self, mock_telephone):
        mock_telephone_saved = Telephone(id_person='residencial',number = "47992036909",type_number = "residencial",person_type = "pessoa juridica")
        mock_telephone.query.get.return_value = mock_telephone_saved
        telephone = get_by_id_person(mock_telephone_saved.id_person)
        self.assertEqual(telephone, mock_telephone_saved)
        mock_telephone.query.get.assert_called_once()



    @patch('app.domains.telephone.actions.Telephone')
    def test_action_get_query_telephone(self, telephone_mock):
        mock_telephone_saved = Telephone(id_person='residencial',number = "47992036909",type_number = "residencial",person_type = "pessoa juridica")
        telephone_mock.query.all.return_value = [mock_telephone_saved]
        all_telephones = get_all()
        self.assertEqual(all_telephones, [mock_telephone_saved])
        telephone_mock.query.all.assert_called_once()




    @patch('app.domains.telephone.actions.get_by_id_person')
    @patch('app.domains.telephone.actions.commit')
    def test_action_telephone_should_be_updated(self, commit_mock, get_by_id_person_mock):
        telephone_mock = Mock()
        data = {'number': '4796968787'}
        get_by_id_person_mock.return_value = telephone_mock

        telephone_updated = update_tel('1', data)

        self.assertEqual(telephone_updated.number, '4796968787')
        get_by_id_person_mock.assert_called_once_with('1')
        commit_mock.assert_called_once()
