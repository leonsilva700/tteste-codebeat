import unittest, pytest
from unittest.mock import Mock
from uuid import uuid4
from app.domains.telephone.models import Telephone
from app.domains.telephone.exceptions import TelephoneInvalidException


class Test_telephone_Models(unittest.TestCase):
    def test_telephone_model_should_be_serialized(self):
        id = str(uuid4())
        telephone = Telephone(id=id, id_person='id_person', number='1234567890',type_number='comercial',person_type='fornecedor')
        json = telephone.serialize()
        self.assertEqual(json['id'], id)
        self.assertEqual(json['number'], '1234567890')
        self.assertEqual(json['type_number'], 'comercial')
        self.assertEqual(json['person_type'], 'fornecedor')

    def test_if_check_telephone_is_not_valid(self):
        with pytest.raises(TelephoneInvalidException) as ex:
            id = str(uuid4())
            Telephone(id=id, id_person='id_person', number='123456789', type_number='comercial',person_type='fornecedor')
        self.assertEqual(ex.value.description, 'O campo telefone só pode ter numeros: no mínimo 10 e no máximo 11')









