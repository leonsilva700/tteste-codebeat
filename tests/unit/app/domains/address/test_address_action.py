import unittest
from unittest.mock import patch, MagicMock, Mock
from app.domains.address.models import Address
from app.domains.address.actions import create, get, get_by_id, update, remove


class TestAddressActions(unittest.TestCase):

    @patch('app.domains.address.actions.save')
    def test_action_create_should_be_created_new_address(self, save_mock):
        create({'zip': '89000200',
                 'street': 'rua teste',
                 'number': 10,
                 'district': 'Bairro teste',
                 'city': 'Cidade teste',
                 'state': 'Estado Teste',
                 'complement': 'complemento teste',
                 'id_person': None})
        calls = save_mock.call_args
        args = calls[0]
        address = args[0]
        save_mock.assert_called_once_with(address)

    @patch('app.domains.address.actions.Address')
    def test_action_get_by_id_should_be_return_address(self, address_mock):
        address_save = Mock()
        query = Mock()
        query.get = MagicMock(return_value=address_save)
        address_mock.query = query
        address = get_by_id('id')
        address_mock.query.get.assert_called_once_with('id')
        self.assertIsNotNone(address)

    @patch('app.domains.address.actions.Address')
    def test_action_get_should_be_return_address(self, address_mock):
        address_saved = Mock()
        query = Mock()
        query.all = MagicMock(return_value=[address_saved])
        address_mock.query = query
        address = get()
        address_mock.query.all.assert_called_once()
        self.assertEqual(len(address), 1)

    @patch('app.domains.address.actions.get_by_id')
    @patch('app.domains.address.actions.commit')
    def test_action_update_should_be_updated_address(self, commit_mock, get_by_id_mock):
        address_saved = Mock()
        get_by_id_mock.return_value = address_saved
        address = update('id', {'zip': '89005600',
                 'street': 'Rua 3',
                 'number': 10,
                 'district': 'Bairro 23',
                 'city': 'Blumenau',
                 'state': 'SC',
                 'complement': 'bloco a',
                 'id_person': None})
        get_by_id_mock.assert_called_once_with('id')
        commit_mock.assert_called_once()
        self.assertEqual(address.zip, '89005600')
        self.assertEqual(address.street, 'Rua 3')
        self.assertEqual(address.number, 10)
        self.assertEqual(address.district, 'Bairro 23')
        self.assertEqual(address.city, 'Blumenau')
        self.assertEqual(address.state, 'SC')
        self.assertEqual(address.complement, 'bloco a')



    @patch('app.domains.address.actions.delete')
    @patch('app.domains.address.actions.get_by_id')
    @patch('app.domains.address.actions.commit')
    def test_action_update_should_be_delete_address(self, commit_mock, get_by_id_mock, delete_mock):
        address_mock = Mock()
        get_by_id_mock.return_value = address_mock
        remove('id')
        get_by_id_mock.assert_called_once_with('id')
        commit_mock.assert_called_once()
        delete_mock.assert_called_once()


