import unittest
from unittest.mock import Mock, MagicMock, patch
from app.domains.address.models import Address
from app.domains.address.exception import AttributeErrorException


class TestAddressModel(unittest.TestCase):

    def setUp(self):
        self._dict_address = {'zip': '89000200',
                 'street': 'rua teste',
                 'number': 10,
                 'district': 'Bairro teste',
                 'city': 'Cidade teste',
                 'state': 'Estado Teste',
                 'complement': 'complemento teste',
                 'id_person': None}

    @patch('app.domains.address.models.uuid4')
    def test_full_dictionary_builder(self, mock_uuid):
        mock_uuid.return_value = '5ad28ced-f5bb-410b-8224-7cab60951b4d'
        address = Address(**self._dict_address)
        self.assertEqual(address.id, '5ad28ced-f5bb-410b-8224-7cab60951b4d')
        self.assertEqual(address.street, 'rua teste')
        self.assertEqual(address.number, 10)
        self.assertEqual(address.district, 'Bairro teste')
        self.assertEqual(address.city, 'Cidade teste')
        self.assertEqual(address.state, 'Estado Teste')
        self.assertEqual(address.complement, 'complemento teste')

    @patch('app.domains.address.models.uuid4')
    def test_builder_with_empty_state_attribute(self, mock_uuid):
        mock_uuid.return_value = '5ad28ced-f5bb-410b-8224-7cab60951b4d'
        dict_ = self._dict_address.copy()
        dict_['state'] = ''
        with self.assertRaises(AttributeErrorException) as ex:
            Address(**dict_)
        self.assertEqual(ex.exception.description, 'Attribute Error - blank or unhealthy address attributes')
        self.assertEqual(ex.exception.code, 422)

    @patch('app.domains.address.models.uuid4')
    def test_builder_with_empty_street_attribute(self, mock_uuid):
        mock_uuid.return_value = '5ad28ced-f5bb-410b-8224-7cab60951b4d'
        dict_ = self._dict_address.copy()
        dict_['street'] = ''
        with self.assertRaises(AttributeErrorException) as ex:
            Address(**dict_)
        self.assertEqual(ex.exception.description, 'Attribute Error - blank or unhealthy address attributes')
        self.assertEqual(ex.exception.code, 422)

    @patch('app.domains.address.models.uuid4')
    def test_builder_with_empty_number_attribute(self, mock_uuid):
        mock_uuid.return_value = '5ad28ced-f5bb-410b-8224-7cab60951b4d'
        dict_ = self._dict_address.copy()
        dict_['number'] = 0
        with self.assertRaises(AttributeErrorException) as ex:
            Address(**dict_)
        self.assertEqual(ex.exception.description, 'Attribute Error - blank or unhealthy address attributes')
        self.assertEqual(ex.exception.code, 422)

    @patch('app.domains.address.models.uuid4')
    def test_builder_with_empty_city_attribute(self, mock_uuid):
        mock_uuid.return_value = '5ad28ced-f5bb-410b-8224-7cab60951b4d'
        dict_ = self._dict_address.copy()
        dict_['city'] = ''
        with self.assertRaises(AttributeErrorException) as ex:
            Address(**dict_)
        self.assertEqual(ex.exception.description, 'Attribute Error - blank or unhealthy address attributes')
        self.assertEqual(ex.exception.code, 422)

    @patch('app.domains.address.models.uuid4')
    def test_builder_with_empty_zip_attribute(self, mock_uuid):
        mock_uuid.return_value = '5ad28ced-f5bb-410b-8224-7cab60951b4d'
        dict_ = self._dict_address.copy()
        dict_['zip'] = ''
        with self.assertRaises(AttributeErrorException) as ex:
            Address(**dict_)
        self.assertEqual(ex.exception.description, 'Attribute Error - blank or unhealthy address attributes')
        self.assertEqual(ex.exception.code, 422)

    @patch('app.domains.address.models.uuid4')
    def test_builder_zip_attribute_with_numbers_and_letters(self, mock_uuid):
        mock_uuid.return_value = '5ad28ced-f5bb-410b-8224-7cab60951b4d'
        dict_ = self._dict_address.copy()
        dict_['zip'] = '456f4sf4'
        with self.assertRaises(AttributeErrorException) as ex:
            Address(**dict_)
        self.assertEqual(ex.exception.description, 'Attribute Error - blank or unhealthy address attributes')
        self.assertEqual(ex.exception.code, 422)

    @patch('app.domains.address.models.uuid4')
    def test_integer_type_attribute_with_string_as_parameter(self, mock_uuid):
        mock_uuid.return_value = '5ad28ced-f5bb-410b-8224-7cab60951b4d'
        dict_ = self._dict_address.copy()
        dict_['number'] = '2'
        with self.assertRaises(AttributeErrorException) as ex:
            Address(**dict_)
        self.assertEqual(ex.exception.description, 'Attribute Error - blank or unhealthy address attributes')
        self.assertEqual(ex.exception.code, 422)

    @patch('app.domains.address.models.uuid4')
    def test_return_method_serialize(self, mock_uuid):
        mock_uuid.return_value = '5ad28ced-f5bb-410b-8224-7cab60951b4d'
        address = Address(**self._dict_address)
        dict_result = {'id': '5ad28ced-f5bb-410b-8224-7cab60951b4d',
                'zip': '89000200',
                 'street': 'rua teste',
                 'number': 10,
                 'district': 'Bairro teste',
                 'city': 'Cidade teste',
                 'state': 'Estado Teste',
                 'complement': 'complemento teste',
                 'id_person': None}
        result = address.serialize()
        self.assertEqual(result, dict_result)

    @patch('app.domains.address.models.uuid4')
    def test_the_generate_id_method_should_return_an_id_size_of_thirty_six_and_format_str(self, mock_uuid):
        mock_uuid.return_value = '5ad28ced-f5bb-410b-8224-7cab60951b4d'
        address = Address(**self._dict_address)
        self.assertEqual(len(address._id_generator()), 36)
        self.assertEqual(type(address._id_generator()), str)








