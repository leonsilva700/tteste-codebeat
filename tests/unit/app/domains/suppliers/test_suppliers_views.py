from pycep_correios.excecoes import ExcecaoPyCEPCorreios

from app.domains.API_correios.exceptions import CheckCepnotavailableException
from app.domains.suppliers.actions import get_by_cep
from app.domains.suppliers.views import get_cep_by_number
from tests.unit import AbstractViewUnitTest
from unittest.mock import patch, MagicMock, Mock


class TestSuppliersViews(AbstractViewUnitTest):

    @patch('app.domains.suppliers.views.get_suppliers')
    def test_get_supplier_should_be_1(self, get_supplier_mock):
        # Arrange
        obj = Mock()
        obj.serialize = MagicMock(return_value={})
        get_supplier_mock.return_value = [obj]

        # Action
        response = self._client.get('/suppliers')
        data = response.get_json()

        # Assertions
        self.assertEqual(len(data), 1)
        get_supplier_mock.assert_called_once()

    @patch('app.domains.suppliers.views.get_supplier_by_id')
    def test_get_supplier_by_id_should_be_1(self, get_supplier_by_id_mock):
        # Arrange
        obj = Mock()
        obj.serialize = MagicMock(return_value={})
        get_supplier_by_id_mock.return_value = obj
        id = '1'

        # Action
        response = self._client.get('/suppliers/{}'.format(id))

        # Assertions
        self.assertEqual(response.status_code, 200)
        get_supplier_by_id_mock.assert_called_once_with(id)

    @patch('app.domains.suppliers.views.create_supplier')
    def test_post_supplier_should_be_created(self, create_supplier_mock):
        # Arrange
        payload = {
            'corporate_name':'AAAAAAA',
            'cnpj':'564456487456456',
            'fantasy_name':'Fantasia',
            'address':'Rua 123',
            'contact_phone': '4564354744',
            'contact_e-mail': 'hkighgkjgjgh',
            'category': 'gighghjggj'
        }
        obj = Mock()
        obj.serialize = MagicMock(return_value={})
        create_supplier_mock.return_value = obj
        # Action
        response = self._client.post('/suppliers', json=payload)

        # Assertions
        self.assertEqual(response.status_code, 201)
        create_supplier_mock.assert_called_once_with({
            'corporate_name': 'AAAAAAA',
            'cnpj': '564456487456456',
            'fantasy_name': 'Fantasia',
            'address': 'Rua 123',
            'contact_phone': '4564354744',
            'contact_e-mail': 'hkighgkjgjgh',
            'category': 'gighghjggj'
        })

    @patch('app.domains.suppliers.views.update_supplier')
    def test_put_supplier_should_be_updated(self, update_supplier_mock):
        # Arrange
        _id = '1'
        obj = Mock()
        obj.serialize = MagicMock(return_value={'contact_e-mail': 'AAA'})
        update_supplier_mock.return_value = obj

        # Action
        response = self._client.put('/suppliers/{}'.format(_id), json={
            'contact_e-mail': 'BBBB',
        })

        # Assertions
        self.assertEqual(response.status_code, 200)
        update_supplier_mock.assert_called_once_with('1', {
            'contact_e-mail': 'BBBB',
        })

    @patch('app.domains.suppliers.views.get_by_cep')
    def test_if_api_will_return_the_address_by_inserting_CEP(self, get_by_cep_mock):
        cep = '89041450'
        return_mock = {"bairro": "Velha",
                       "cep": "89041450",
                       "cidade": "Blumenau",
                       "complemento2": None,
                       "end": "Rua Reinoldo Althoff",
                       "uf": "SC",
                       "unidadesPostagem": []}

        get_by_cep_mock.return_value=return_mock
        response = self._client.get('/suppliers/cep/{}'.format(cep))
        get_by_cep_mock.mock_called_once_with(cep)

