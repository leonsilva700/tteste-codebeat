import unittest
from unittest.mock import patch, MagicMock, Mock

from pycep_correios.excecoes import ExcecaoPyCEPCorreios

from app.domains.API_correios.exceptions import CheckCepnotavailableException
from app.domains.suppliers.actions import get_by_id, update, create, get, get_by_cep
from app.domains.suppliers.models import Suppliers


class TestUsersActions(unittest.TestCase):
    @patch('app.domains.suppliers.actions.save')
    def test_action_create_should_be_created_new_supplier(self, save_mock):
        create({'corporate_name': 'teste'})
        save_mock.assert_called_once()
        calls = save_mock.call_args
        args = calls[0]
        suppliers = args[0]
        self.assertIsInstance(suppliers, Suppliers)
        self.assertEqual(suppliers.corporate_name, 'teste')

    @patch('app.domains.suppliers.actions.Suppliers')
    def test_action_get_by_id_should_be_return_supplier(self, supplier_mock):
        # Arrange
        mock_dict = MagicMock(return_value={})
        supplier_saved = Suppliers(**mock_dict)
        supplier_saved.corporate_name = 'Name'
        query = Mock()
        query.get = MagicMock(return_value=supplier_saved)
        supplier_mock.query = query
        supplier = get_by_id('id')
        supplier_mock.query.get.assert_called_once_with('id')
        self.assertEqual(supplier_saved, supplier)

    @patch('app.domains.suppliers.actions.Suppliers')
    def test_action_get_should_be_return_suppliers(self, suppliers_mock):
        mock_dict = MagicMock(return_value={})
        supplier_saved = Suppliers(**mock_dict)
        supplier_saved.corporate_name = 'Teste Name'
        query = Mock()
        query.all = MagicMock(return_value=[supplier_saved])
        suppliers_mock.query = query
        suppliers = get()
        self.assertTrue(suppliers_mock.query.all.called)
        self.assertEqual(len(suppliers), 1)

    @patch('app.domains.suppliers.actions.update_email')
    @patch('app.domains.suppliers.actions.update_telephone')
    @patch('app.domains.suppliers.actions.update_address')
    @patch('app.domains.suppliers.actions.get_by_id')
    @patch('app.domains.suppliers.actions.commit')
    def test_action_update_should_be_updated_suppliers(self, commit_mock, get_by_id_mock, update_address_mock, update_telephone_mock, update_email_mock):
        mock_dict = MagicMock(return_value={})
        supplier_saved = Suppliers(**mock_dict)
        supplier_saved.fantasy_name = 'Teste Name'
        get_by_id_mock.return_value = supplier_saved
        dict_ = {'fantasy_name': 'Name'}
        supplier = update('id', dict_)
        get_by_id_mock.assert_called_once_with('id')
        update_address_mock.assert_called_once()
        update_telephone_mock.assert_called_once()
        update_email_mock.assert_called_once()
        self.assertTrue(commit_mock.called)
        self.assertEqual(supplier.fantasy_name, 'Name')

    @patch('app.domains.suppliers.actions.get_address_by_cep_number')
    def test_if_the_address_will_return_when_searching_by_cep(self,get_address_by_cep_number_mock):
        cep = '89041450'
        return_mock = {"bairro": "Velha",
                   "cep": "89041450",
                   "cidade": "Blumenau",
                   "complemento2": None,
                   "end": "Rua Reinoldo Althoff",
                   "uf": "SC",
                   "unidadesPostagem": []}
        get_address_by_cep_number_mock = MagicMock(return_value = return_mock)
        get_by_cep(cep)
        get_address_by_cep_number_mock.mock_called_once_with(cep)

    @patch('app.domains.suppliers.actions.get_address_by_cep_number')
    def test_if_no_address_will_be_returned_when_searching_by_cep_once_its_invalid(self,get_address_by_cep_number_mock):
        cep = '8904145'
        get_address_by_cep_number_mock.side_effect = ExcecaoPyCEPCorreios()
        with self.assertRaises(ExcecaoPyCEPCorreios) as exc:
            get_by_cep(cep)
        exc = exc.exception
        self.assertEqual(exc.message, 'O CEP está errado')
        get_address_by_cep_number_mock.mock_called_once_with(cep)



