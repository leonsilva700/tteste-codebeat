from app.exceptions import UnprocessableException


class InvalidCNPJException(UnprocessableException):
    def __init__(self, msg: str = 'Invalid CNPJ'):
        super().__init__(msg=msg)


class EmptyCNPJException(UnprocessableException):
    def __init__(self, msg: str = 'Empty CNPJ'):
        super().__init__(msg=msg)
