from app.domains.cnpj.models import CNPJ


def create(data: str) -> CNPJ:
    return CNPJ(data)
