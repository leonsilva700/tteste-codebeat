from bradocs4py import ValidadorCnpj
from app.domains.cnpj.exceptions import InvalidCNPJException, EmptyCNPJException
import re


def _get_only_numbers(value: str) -> str:
    return re.sub('[^0-9]', '', value)


class CNPJ:
    def __init__(self, value: str):
        CNPJ._validate_empty_value(value)
        CNPJ._validate(value)
        self._value = _get_only_numbers(value)

    @staticmethod
    def _validate(value: str) -> None:
        if not ValidadorCnpj.validar(value):
            raise InvalidCNPJException()

    @staticmethod
    def _validate_empty_value(value: str) -> None:
        if value is None or not len(value.replace(' ', '')):
            raise EmptyCNPJException()

    def get_value(self) -> str:
        return self._value

