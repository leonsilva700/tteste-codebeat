from app.domains.address.models import Address
from database.repository import save, commit, delete


def create(data: dict) -> Address:
    return save(Address(**data))


def get() -> list:
    return Address.query.all()


def get_by_id(id: str) -> Address:
    return Address.query.get(id)


def update(id: str, data: dict) -> Address:
    address = get_by_id(id)
    address.zip = data.get('zip')
    address.street = data.get('street')
    address.number = data.get('number')
    address.district = data.get('district')
    address.city = data.get('city')
    address.state = data.get('state')
    address.complement = data.get('complement')
    commit()
    return address


def remove(id: str) -> None:
    address = get_by_id(id)
    delete(address)
    commit()


