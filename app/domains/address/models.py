from uuid import uuid4
from database import db
import re
from app.domains.address.exception import AttributeErrorException
from typing import Final

ZIP_COUNT: Final = 8
ZIP_REGEX: Final = u'[^0-9: ]'


class Address(db.Model):

    __tablename__ = 'address'

    id = db.Column(db.String(36), primary_key=True)
    zip = db.Column(db.String(80))
    street = db.Column(db.String(80))
    number = db.Column(db.Integer)
    district = db.Column(db.String(40))
    city = db.Column(db.String(40))
    state = db.Column(db.String(40))
    complement = db.Column(db.String(80))
    id_person = db.Column(db.String(36))

    def __init__(self, **kwargs):
        super(Address, self).__init__(**kwargs)
        self._validate_not_null_and_none(kwargs)
        self._validate_zip(kwargs['zip'])
        self.id = self._id_generator()

    def _validate_not_null_and_none(self, address: dict) -> None:
        if not (address['zip'] and address['street'] and address['number'] and address['city'] and address['state']):
            raise AttributeErrorException()
        if not isinstance(address['number'], int):
            raise AttributeErrorException()

    def _id_generator(self) -> str:
        return str(uuid4())

    def _validate_zip(self, zip_: str) -> None:
        zip_ = re.sub(ZIP_REGEX, '', zip_)
        if len(zip_) != ZIP_COUNT:
            raise AttributeErrorException()
        self.zip = zip_

    def serialize(self) -> dict:
        return {
            'id': self.id,
            'zip': self.zip,
            'street': self.street,
            'number': self.number,
            'district': self.district,
            'city': self.city,
            'state': self.state,
            'complement': self.complement,
            'id_person': self.id_person
        }



