from app.exceptions import UnprocessableException


class AttributeErrorException(UnprocessableException):
    def __init__(self, msg: str = 'Attribute Error - blank or unhealthy address attributes'):
        super().__init__(msg=msg)
