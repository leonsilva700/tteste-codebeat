from app.domains.API_correios.actions import get_address_by_cep_number
from app.domains.suppliers.models import Suppliers
from database.repository import save, commit
from uuid import uuid4
from app.domains.address.actions import update as update_address
from app.domains.telephone.actions import update as update_telephone
from app.domains.email.actions import update as update_email


def create(data: dict) -> Suppliers:
    return save(Suppliers(id=str(uuid4()), **data))


def get() -> list:
    return Suppliers.query.all()


def get_by_id(id: str) -> Suppliers:
    return Suppliers.query.get(id)


def update(id: str, data: dict) -> Suppliers:
    supplier = get_by_id(id)
    supplier.fantasy_name = data.get('fantasy_name')
    supplier.address = update_address(id, data.get('address'))
    supplier.phone_number = update_telephone(id, data.get('telephone'))
    supplier.email_address = update_email(id, data.get('email'))
    commit()
    return supplier

def get_by_cep(cep: str) -> dict:
    return get_address_by_cep_number(cep)

