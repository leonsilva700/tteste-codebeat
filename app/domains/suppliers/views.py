from flask import Blueprint, jsonify, request

app_suppliers = Blueprint('app.suppliers', __name__)

from app.domains.suppliers.actions import \
    get as get_suppliers, \
    create as create_supplier, \
    update as update_supplier, \
    get_by_id as get_supplier_by_id, get_by_cep


@app_suppliers.route('/suppliers', methods=['POST'])
def post():
    payload = request.get_json()
    supplier = create_supplier(payload)
    return supplier.serialize(), 201


@app_suppliers.route('/suppliers/<id>', methods=['PUT'])
def put(id):
    payload = request.get_json()
    supplier = update_supplier(id, payload)
    return supplier.serialize(), 200


@app_suppliers.route('/suppliers', methods=['GET'])
def get():
    return jsonify([supplier.serialize() for supplier in get_suppliers()]), 200


@app_suppliers.route('/suppliers/<id>', methods=['GET'])
def get_by_id(id):
    supplier = get_supplier_by_id(id)
    return jsonify(supplier.serialize()), 200

@app_suppliers.route('/suppliers/cep/<cep>', methods=['GET'])
def get_cep_by_number(cep):
    cep = get_by_cep(cep)
    return jsonify(cep), 200