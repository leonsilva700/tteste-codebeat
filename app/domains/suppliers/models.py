from database import db


class Suppliers(db.Model):
    __tablename__ = 'suppliers'

    id = db.Column(db.String(36), primary_key=True)
    corporate_name = db.Column(db.String(120))
    fantasy_name = db.Column(db.String(120))
    address = db.Column(db.String(120))
    phone_number = db.Column(db.String(13))
    email_address = db.Column(db.String(120))

