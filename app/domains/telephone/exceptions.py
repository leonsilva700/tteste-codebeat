from app.exceptions import UnprocessableException


class TelephoneInvalidException(UnprocessableException):
    def __init__(self, msg: str = "O campo telefone só pode ter numeros: no mínimo 10 e no máximo 11"):
        super().__init__(msg=msg)



