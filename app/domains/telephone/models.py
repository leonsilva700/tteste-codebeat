from app.domains.telephone.exceptions import TelephoneInvalidException

from database import db

minimo_de_numeros = 10
maximo_de_numeros = 11

class Telephone(db.Model):
    __tablename__ = 'telephone'

    id = db.Column(db.String(36), primary_key=True)
    id_person = db.Column(db.String(36))
    number = db.Column(db.String(15))
    type_number = db.Column(db.String(15))
    person_type = db.Column(db.String(30))

    def _check_telephone_valid(self, number: str) -> None:
        if not (number.isnumeric() and len(number) >= minimo_de_numeros and len(number) <= maximo_de_numeros):
            raise TelephoneInvalidException()

    def __init__(self, **kwargs):
        super(Telephone,self).__init__(**kwargs)
        self._check_telephone_valid(kwargs['number'])

    def serialize(self) -> dict:
        return {
            'id': self.id,
            'id_person': self.id_person,
            'number': self.number,
            'type_number': self.type_number,
            'person_type': self.person_type

        }




