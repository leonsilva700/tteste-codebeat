from app.domains.telephone.models import Telephone
from database.repository import save, commit
from uuid import uuid4


def create(data: str) -> Telephone:
        return save(Telephone(id=str(uuid4()),
        id_person=data['id_person'],
        number=data['number'],
        type_number=data['type_number'],
        person_type=data['person_type'])
                    )
def get() -> list:
    return Telephone.query.all()


def get_by_id_person(id_person: str) -> Telephone:
    return Telephone.query.get(id_person=id_person)


def update(id_person, data: dict) -> Telephone:
    telephone = get_by_id_person(id_person)
    telephone.number = data.get('number')
    commit()
    return telephone
