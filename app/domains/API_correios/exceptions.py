from app.exceptions import UnprocessableException


class CheckCepnotavailableException(UnprocessableException):
    def __init__(self, msg: str = 'O CEP está errado'):
        super().__init__(msg=msg)