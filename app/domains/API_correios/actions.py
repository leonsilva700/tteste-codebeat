import pycep_correios
from pycep_correios.excecoes import ExcecaoPyCEPCorreios
from app.domains.API_correios.exceptions import CheckCepnotavailableException


def get_address_by_cep_number(cep_number: str) -> dict:
    try:
        address = pycep_correios.consultar_cep(cep_number)
    except ExcecaoPyCEPCorreios:
        raise CheckCepnotavailableException
    return address

