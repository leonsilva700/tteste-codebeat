from app.domains.category.models import Category
from database.repository import save, commit


def create(data: dict) -> Category:
    return save(Category(**data))


def get() -> list:
    return Category.query.all()


def get_by_id(id: str) -> Category:
    return Category.query.get(id)


def update(id: str, data: dict) -> Category:
    category = get_by_id(id)
    category.name = data.get('name')
    category.active = data.get('active')
    commit()
    return category
