from uuid import uuid4


def uuid4_generator() -> str:
    return str(uuid4())
