import re


def regex_only_numbers(data: str) -> str:
    return re.sub(u'[^0-9: ]', '', data)
