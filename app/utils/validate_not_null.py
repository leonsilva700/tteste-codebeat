from typing import TypeVar, Iterable, Tuple

T = TypeVar(int, float, str, bool)
Vector = Iterable[Tuple[T, T]]


def validate_not_null_dict(keys: list, dict_: dict) -> bool:
    for i in keys:
        if not validate_not_empty_or_none(dict_[i]):
            return False
    return True


def validate_not_null_object(obj_: object) -> bool:
    for i in obj_:
        if not validate_not_empty_or_none(i):
            return False
    return True


def validate_not_empty_or_none(data: Vector[T]) -> bool:
    if not bool(data):
        return False
    return True